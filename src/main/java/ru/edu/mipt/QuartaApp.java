package ru.edu.mipt;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static java.lang.String.*;

public class QuartaApp {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJarByClass(QuartaApp.class);
        job.setMapperClass(QuartaMapper.class);
        job.setReducerClass(QuartileReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        job.setNumReduceTasks(1);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

    public static class QuartaMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] splited = value.toString().split("\\s");
            long id = Long.valueOf(splited[0]);
            long timestamp = Long.valueOf(splited[1]);
            String host = new URL(splited[2]).getHost();
            long diffTime = Long.valueOf(splited[3].split(":")[1]);

            System.out.println(format("id: %d, timestamp: %d, host: %s, diffTime: %d", id, timestamp, host, diffTime));

            context.write(new Text(host), new LongWritable(diffTime));
        }
    }

    public static class UUIDCombiner extends Reducer<IntWritable, Text, IntWritable, Text> {

        @Override
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            int basket = new Random().nextInt(context.getNumReduceTasks()) + 1;
            for (Text value : values)
                context.write(new IntWritable(basket), value);
        }

    }

    public static class QuartileReducer extends Reducer<Text, LongWritable, Text, Text> {

        private final QuartileCalculator quartileCalculator = new QuartileCalculator();

        @Override
        public void reduce(Text key, Iterable<LongWritable> writables, Context context) throws IOException, InterruptedException {
            Iterator<LongWritable> iterator = writables.iterator();
            ArrayList<Long> values = new ArrayList<>();
            while (iterator.hasNext())
                values.add(iterator.next().get());


            long mediane = quartileCalculator.mediane(values);
            long thirdQuartile = quartileCalculator.thirdQuartile(values);

            int medianeCounter = 0;
            int thirdQuartileCounter = 0;

            for (Long value : values) {
                if (value <= mediane) medianeCounter++;
                if (value <= thirdQuartile) thirdQuartileCounter++;
            }
            context.write(key, new Text(format("%d %d", medianeCounter, thirdQuartileCounter)));
        }
    }

    public static class QuartileCalculator {

        public long mediane(List<Long> list) {
            return list.get(list.size() / 2);
        }

        public long thirdQuartile(List<Long> list) {
            return list.get((int) (list.size() * 0.75));
        }

    }

}